VANTA.NET({
el: "#main-wrap",
mouseControls: true,
touchControls: true,
gyroControls: false,
minHeight: 200.00,
minWidth: 200.00,
scale: 1.00,
scaleMobile: 1.00,
color: 0x3bff,
points: 12.00
})

$( document ).ready(function() {
    AOS.init();
    
    // worldchefs
    $('#one').click(function(){
      $('#modal-worldchefs').removeClass('out');
      $('#modal-worldchefs').addClass('openss');
      $('body').addClass('modal-active');
    })
    $('#modal-worldchefs').click(function(){
        $(this).addClass('out');
        $(this).removeClass('openss');
        $('body').removeClass('modal-active');
    });

    // true protection
    $('#two').click(function(){
      var buttonId = $(this).attr('id');
      $('#modal-trueprotect').removeClass('out');
      $('#modal-trueprotect').addClass('openss');
      $('body').addClass('modal-active');
    })
    $('#modal-trueprotect').click(function(){
        $(this).addClass('out');
        $(this).removeClass('openss');
        $('body').removeClass('modal-active');
    });

    // people's care
    $('#three').click(function(){
      var buttonId = $(this).attr('id');
      $('#modal-peoplescare').removeClass('out');
      $('#modal-peoplescare').addClass('openss');
      $('body').addClass('modal-active');
    })
    $('#modal-peoplescare').click(function(){
        $(this).addClass('out');
        $(this).removeClass('openss');
        $('body').removeClass('modal-active');
    });

    // siam secure
    $('#four').click(function(){
      var buttonId = $(this).attr('id');
      $('#modal-siamsecure').removeClass('out');
      $('#modal-siamsecure').addClass('openss');
      $('body').addClass('modal-active');
    })
    $('#modal-siamsecure').click(function(){
        $(this).addClass('out');
        $(this).removeClass('openss');
        $('body').removeClass('modal-active');
    });

    // iica
    $('#five').click(function(){
      var buttonId = $(this).attr('id');
      $('#modal-iica').removeClass('out');
      $('#modal-iica').addClass('openss');
      $('body').addClass('modal-active');
    })
    $('#modal-iica').click(function(){
        $(this).addClass('out');
        $(this).removeClass('openss');
        $('body').removeClass('modal-active');
    });

    // truedev
    $('#six').click(function(){
      var buttonId = $(this).attr('id');
      $('#modal-truedev').removeClass('out');
      $('#modal-truedev').addClass('openss');
      $('body').addClass('modal-active');
    })
    $('#modal-truedev').click(function(){
        $(this).addClass('out');
        $(this).removeClass('openss');
        $('body').removeClass('modal-active');
    });

    // hungrynow
    $('#seven').click(function(){
      var buttonId = $(this).attr('id');
      $('#modal-hn').removeClass('out');
      $('#modal-hn').addClass('openss');
      $('body').addClass('modal-active');
    })
    $('#modal-hn').click(function(){
        $(this).addClass('out');
        $(this).removeClass('openss');
        $('body').removeClass('modal-active');
    });

    // mauka
    $('#eight').click(function(){
      var buttonId = $(this).attr('id');
      $('#modal-mauka').removeClass('out');
      $('#modal-mauka').addClass('openss');
      $('body').addClass('modal-active');
    })
    $('#modal-mauka').click(function(){
        $(this).addClass('out');
        $(this).removeClass('openss');
        $('body').removeClass('modal-active');
    });

    // fdp
    $('#nine').click(function(){
      var buttonId = $(this).attr('id');
      $('#modal-ftp').removeClass('out');
      $('#modal-ftp').addClass('openss');
      $('body').addClass('modal-active');
    })
    $('#modal-ftp').click(function(){
        $(this).addClass('out');
        $(this).removeClass('openss');
        $('body').removeClass('modal-active');
    });

    // dolphin
    $('#ten').click(function(){
      var buttonId = $(this).attr('id');
      $('#modal-dolphin').removeClass('out');
      $('#modal-dolphin').addClass('openss');
      $('body').addClass('modal-active');
    })
    $('#modal-dolphin').click(function(){
        $(this).addClass('out');
        $(this).removeClass('openss');
        $('body').removeClass('modal-active');
    });

});
